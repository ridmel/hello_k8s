import os

from dotenv import load_dotenv


load_dotenv()
name = os.getenv("NAME", default="k8s")


def concatenate(word: str) -> str:
    return "Hello " + word + "!"


if __name__ == "__main__":
    print(concatenate(name))
