# Hello k8s!
A simple example demonstrating how to work with a gitlab ci/cd pipeline using Docker and Kubernetes. 
The application simply prints `Hello <your word>!`, but all stages of the pipeline are working and presented: 
lint and test, build and deploy.