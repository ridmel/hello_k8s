FROM python:3.11.5-bookworm as base-stage

WORKDIR /code

RUN python -m pip install --no-cache-dir --upgrade pip


FROM base-stage as prod-build

COPY requirements.txt ./
RUN python -m pip install --no-cache-dir -r /code/requirements.txt
COPY app.py ./

ENTRYPOINT python app.py


FROM base-stage as test-build

COPY requirements_dev.txt ./
RUN python -m pip install --no-cache-dir -r /code/requirements_dev.txt
COPY app.py .flake8 test_app.py .env ./

ENTRYPOINT flake8 && pytest && echo "All checks successful!"